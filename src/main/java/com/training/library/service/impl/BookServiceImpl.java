package com.training.library.service.impl;

import com.training.library.converter.BookConverter;
import com.training.library.dto.BookDto;
import com.training.library.model.Book;
import com.training.library.repository.BookRepository;
import com.training.library.service.BookService;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("BookService")
public class BookServiceImpl implements BookService {

  private final BookRepository bookRepository;
  private final BookConverter bookConverter;

  @Autowired
  public BookServiceImpl(BookRepository bookRepository, BookConverter bookConverter) {
    this.bookRepository = bookRepository;
    this.bookConverter = bookConverter;
  }

  @Override
  public Book save(BookDto bookDto) {
    Book book = bookConverter.fromDto(bookDto);

    return bookRepository.save(book);
  }

  @Override
  public Book getById(Long id) {
    return bookRepository.findById(id)
        .orElseThrow(EntityNotFoundException::new);
  }

  @Override
  public List<Book> getAll() {
    return bookRepository.findAll();
  }

  @Override
  public Book update(Long id, BookDto bookDto) {
    Book book = bookConverter.fromDto(bookDto);
    book.setId(id);

    return bookRepository.save(book);
  }

  @Override
  public void deleteById(Long id) {
    bookRepository.deleteById(id);
  }
}
