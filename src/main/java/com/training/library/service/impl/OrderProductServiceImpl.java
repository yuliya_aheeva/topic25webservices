package com.training.library.service.impl;

import com.training.library.converter.OrderProductConverter;
import com.training.library.dto.OrderProductDto;
import com.training.library.model.Order;
import com.training.library.model.OrderProduct;
import com.training.library.repository.OrderProductRepository;
import com.training.library.service.OrderProductService;
import com.training.library.service.OrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderProductServiceImpl implements OrderProductService {

    private final OrderProductRepository orderProductRepository;
    private final OrderProductConverter orderProductConverter;

    private final OrderService orderService;

    public OrderProductServiceImpl(OrderProductRepository orderProductRepository,
                                   OrderProductConverter orderProductConverter,
                                   OrderService orderService) {
        this.orderProductRepository = orderProductRepository;
        this.orderProductConverter = orderProductConverter;
        this.orderService = orderService;
    }

    @Override
    public OrderProduct save(OrderProductDto orderProductDto) {
        OrderProduct orderProduct = orderProductConverter.fromDto(orderProductDto);
        Order order = orderProduct.getOrderId();
        Double totalPrice = order.getTotalPrice() + orderProduct.getProductId().getPrice();
        order.setTotalPrice(totalPrice);
        orderService.update(order);
        return orderProductRepository.save(orderProduct);
    }
}
