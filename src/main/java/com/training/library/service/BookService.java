package com.training.library.service;

import com.training.library.dto.BookDto;
import com.training.library.model.Book;
import java.util.List;

public interface BookService {

  Book save(BookDto bookDto);

  Book getById(Long id);

  List<Book> getAll();

  Book update(Long id, BookDto bookDto);

  void deleteById(Long id);
}
