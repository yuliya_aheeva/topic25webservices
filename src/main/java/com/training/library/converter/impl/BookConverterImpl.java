package com.training.library.converter.impl;

import com.training.library.converter.BookConverter;
import com.training.library.dto.BookDto;
import com.training.library.model.Book;
import org.springframework.stereotype.Component;

@Component
public class BookConverterImpl implements BookConverter {

  @Override
  public Book fromDto(BookDto bookDto) {
    Book book = new Book();

    book.setTitle(bookDto.getTitle());
    book.setAuthor(bookDto.getAuthor());

    return book;
  }
}
