package com.training.library.converter;

import com.training.library.dto.BookDto;
import com.training.library.model.Book;

public interface BookConverter {

  Book fromDto(BookDto bookDto);
}
