package com.training.library.dto;

import lombok.Getter;

import javax.validation.constraints.Size;

@Getter
public class OrderProductDto {
    @Size(min = 1, message = "orderId must be not blank")
    private long orderId;

    @Size(min = 1, message = "productId must be not blank")
    private long productId;
}
