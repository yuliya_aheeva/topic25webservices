package com.training.library.dto;

import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class BookDto {

  @Size(min = 4, max = 64, message = "Title must be between 4 and 64")
  private String title;

  @NotBlank(message = "Author must be not blank")
  private String author;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookDto bookDto = (BookDto) o;
    return Objects.equals(getTitle(), bookDto.getTitle()) && Objects
        .equals(getAuthor(), bookDto.getAuthor());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getTitle(), getAuthor());
  }

  @Override
  public String toString() {
    return "BookDto{" +
        "title='" + title + '\'' +
        ", author='" + author + '\'' +
        '}';
  }
}
