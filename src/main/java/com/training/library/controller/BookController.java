package com.training.library.controller;

import static org.springframework.http.HttpStatus.CREATED;

import com.training.library.dto.BookDto;
import com.training.library.model.Book;
import com.training.library.service.BookService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(value = "/books")
public class BookController {

  private final BookService bookService;

  @Autowired
  public BookController(BookService bookService) {
    this.bookService = bookService;
  }

  @PostMapping
  public ResponseEntity<Book> save(@Valid @RequestBody BookDto bookDto) {
    Book savedBook = bookService.save(bookDto);

    String currentUri = ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString();
    String savedBookLocation = currentUri + "/" + savedBook.getId();

    return ResponseEntity.status(CREATED)
        .header(HttpHeaders.LOCATION, savedBookLocation)
        .body(savedBook);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Book> getById(@PathVariable("id") Long id) {
    Book book = bookService.getById(id);

    return ResponseEntity.ok(book);
  }

  @GetMapping
  public ResponseEntity<List<Book>> getAll() {
    List<Book> books = bookService.getAll();

    return ResponseEntity.ok(books);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Book> update(
      @PathVariable("id") Long id,
      @Valid @RequestBody BookDto bookDto
  ) {
    Book updatedBook = bookService.update(id, bookDto);

    return ResponseEntity.ok(updatedBook);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
    bookService.deleteById(id);

    return ResponseEntity.ok().build();
  }
}
