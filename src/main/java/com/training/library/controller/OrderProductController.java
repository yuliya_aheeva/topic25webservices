package com.training.library.controller;

import com.training.library.dto.BookDto;
import com.training.library.dto.OrderProductDto;
import com.training.library.model.Book;
import com.training.library.model.OrderProduct;
import com.training.library.service.OrderProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping(value = "/orders-products")
public class OrderProductController {
    private final OrderProductService orderProductService;

    public OrderProductController(OrderProductService orderProductService) {
        this.orderProductService = orderProductService;
    }

    @PostMapping
    public ResponseEntity<OrderProduct> save(@Valid @RequestBody OrderProductDto orderProductDto) {
        OrderProduct savedOrderProduct = orderProductService.save(orderProductDto);

        String currentUri = ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString();
        String savedBookLocation = currentUri + "/" + savedOrderProduct.getId();

        return ResponseEntity.status(CREATED)
                .header(HttpHeaders.LOCATION, savedBookLocation)
                .body(savedOrderProduct);
    }
}
